'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('default', function () {
    gulp.start('serve');
});

gulp.task('serve', function () {
  browserSync.init({
      server: {
          baseDir: "app"
      }
  });
  
  gulp.watch("app/{,*/}*.*", browserSync.reload);
});