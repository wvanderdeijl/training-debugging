(function() {
  'use strict';
  
  var i = 0;
  function first() {
    console.trace('first()');
    return i++;
  }
  
  function second() {
    return "Index: " + first();
  }
  
  function third() {
    return {first: second(), second: second()};
  }
  
  function fourth() {
    return [third()];
  }
  
  function fifth() {
    return fourth().concat(fourth());
  }
  
  function callMe() {
    console.groupCollapsed('Table');
    console.table(fifth().concat(fifth()));
    console.groupEnd();
  }
  window.callMe = callMe;
}());