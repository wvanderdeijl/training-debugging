(function(){
  
  function unHide() {
    
    reveal(document.querySelector(".hidden"));
    
    window.addEventListener('click', function() {
      var invisible = document.querySelector('.invisible');
      invisible.classList.remove('invisble');
      
      invisible.innerText = invisible.innerText || 'Woohoow, dat was m alweer!';
    });
  }
  
  window.addEventListener('click', unHide);
  
  window.addEventListener('click', removeClick);
  function removeClick(e) {
    e.stopImmediatePropagation();
    var click = document.querySelector("#click");
    click.parentNode.removeChild(click);
    window.removeEventListener('click', removeClick);
  };
  

}());