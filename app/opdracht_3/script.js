function test(){
    var pietersen = getPietersen();
    for (var index = 0; index < pietersen.length; index++){
        console.log("Pietersen %s: %O", index, pietersen[index]);
    }
}
function getPietersen(){
    var pietersen = [];
    var werknemers = getWerknemers();
    for (var index = 0; index < werknemers.length; index++){
        if (werknemers[index].achternaam === "Pietersen"){
            if (pietersen === null){
                pietersen = [];
            }
            pietersen.push(werknemers[index]);
        }
    }
    return pietersen;
}
function getWerknemers(){
    return [
        {
            voornaam: "Jan",
            achternaam: "Jansen"
        },
        {   voornaam: "Erik",
            achternaam: "Merkens"
        },
        {
            voornaam: "Anja",
            achternaam: "Goris"
        },
        {
            voornaam: "Lucas",
            achternaam: "Nederhoed"
        },
        {  
            voornaam: "Lonnie",
            achternaam:"Jansen"
        }
    ];
}
test();