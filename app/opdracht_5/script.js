(function(){
  'use strict';
  
  function profile() {
    console.profile('My Profile');
    console.time('My Timer');
    
    useMem([]);
  }
  
  function useMem(arr) {
    for (var i = 0; i < 10000; i++) {
      arr.push("Zinnetje " + i);
    }
    
    console.assert(arr.length <= 400000, 'Oeps, we zijn wat lang, laten we stoppen!');
    if(arr.length > 2000000) {
      console.profileEnd('My Profile');
      console.timeEnd('My Timer');
    } else {
      setTimeout(function() {
        useMem(arr);
      }, 3);
    }
  }
  
  window.profile = profile;
}());