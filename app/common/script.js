var opdrachten = 11;

document.addEventListener('DOMContentLoaded', function(event){
  'use strict';
  
  var num = Number(/opdracht_(\d+)/.exec(window.location.pathname)[1]);
  if(isNaN(num)){
    return;
  }
  
  
  var title = "Opdracht " + num;
  
  var firstChild = document.body.children[0];
  var h = document.createElement('h1');
  h.innerText = title;
  if(firstChild) {
    document.body.insertBefore(h, firstChild);
  } else {
    document.body.appendChild(h);
  }
  document.head.appendChild(document.createElement('title')).innerText = title;

  setTimeout(function() {
    window.click = null;
  }, 50);
  
  var footer = document.createElement('footer');
  if(num > 1) {
    var previousLink = '/opdracht_' + (num - 1) + '/';
    footer.innerHTML += '<a class="previous" href="' + previousLink + '">Previous</a>';
  }
  if(num < opdrachten) {
    var nextLink = '/opdracht_' + (num + 1) + '/';
    footer.innerHTML += '<a class="next" href="' + nextLink + '">Next</a>';
  }
  document.body.appendChild(footer);
  
  window.addEventListener('keydown', function(event) {
    switch(event.which) {
    case 39: // Right Arrow
      if(nextLink) {
        window.location.assign(nextLink);
      }
      break;
    case 37: // Left Arrow
      if(previousLink) {
        window.location.assign(previousLink);
      }
      break;
    }
  });
});

function hide(element) {
  if(element) {
    element.classList.add("hidden");
  }
}

function reveal(element) {
  if(element) {
    element.classList.remove("hidden");
  }
}
